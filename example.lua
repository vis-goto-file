local function h(rest)
	return os.getenv"HOME" .. rest
end

-- join manually, as {unpack(a), unpack(b), ...} doesn't work the way I need it to
local function j(t)
	local unpacked = {}
	for _, path in ipairs(t) do
		if type(path) == "table" then
			for _, dir in ipairs(path) do
				table.insert(unpacked, dir)
			end
		else
			table.insert(unpacked, path)
		end
	end
	return unpacked
end

local function pkg_config(name)
	return load('return {' ..
		io.popen("pkg-config --cflags-only-I " .. name)
		:read():gsub("-I([^ ]+)", '"%1",')
	.. '}')()
end

local function venv(name)
	return h"/.local/share/virtualenvs/" .. name .. "/lib/python3.9/site-packages"
end


return {
	["/"] = {
		python = {"/usr/lib/python3.9"},
		lua = {"/usr/share/lua/5.4"},
	},
	[h"/src/gtk2-program"] = {
		ansi_c = j{pkg_config"gtk+-2.0", pkg_config"dbus-glib-1"},
	},
	[h"/src/py-program"] = {
		python = {venv"py-program-IXUzgxSN"},
	},
}
